<?php
/**
 * Created by PhpStorm.
 * User: kovma
 * Date: 2018. 07. 04.
 * Time: 10:54
 */

class ErrorController extends Controller
{
    function __construct()
    {
        parent::__construct();
//        echo 'This is an error page';
        $this->view->msg="This page doesn't exists";
        $this-> view->render('error/index');
    }
}