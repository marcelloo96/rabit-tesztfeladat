<?php
/**
 * Created by PhpStorm.
 * User: kovma
 * Date: 2018. 07. 04.
 * Time: 10:45
 */

class Loader
{

    /**
     * Loader constructor.
     * 1, We modify the url first so it can not end with "/"
     * The 1st element of the url array ([0]) is the name of the Controller, so we pass only the 1st ([0]) element of the url to call the Controller.
     * If the passed Controller doesn't exists then we call an Error page.
     */
    function __construct()
    {
        $url = $_GET['url'];
        $url = rtrim($url,'/');
        $url = explode('/', $url);


        $file = 'Controllers/'.$url[0].'.php';
        if(file_exists($file)){
            require $file;
        }else{
            require "Controllers/ErrorController.php";
            $controller = new ErrorController();
            return false;
        }

        $controller = new $url[0];
    }

}